
IF(UNIX AND NOT APPLE)
	FIND_LIBRARY(X264 NAMES libx264.so)
	SET(X264_LIBRARY_PATH "/usr/local/lib/libx264.so")
	SET(X264_INCLUDE_PATH "/usr/local/include/")
ENDIF(UNIX AND NOT APPLE)

IF(APPLE) 
	FIND_LIBRARY(X264 NAMES libx264.dylib PATHS /opt/local/lib /usr/local/lib)
	SET(X264_LIBRARY_PATH ${X264})
	SET(X264_INCLUDE_PATH "/usr/local/include/")
ENDIF(APPLE)


IF(WIN32)
	FIND_LIBRARY(X264 NAMES libx264-128.dll PATHS "C:/Users/Greenflow/x264/lib")
        FIND_LIBRARY(zlib1 NAMES zlib1.dll PATHS "C:/Windows/System32")
	SET(X264_LIBRARY_PATH ${X264})
	SET(X264_INCLUDE_PATH "C:/Users/Greenflow/x264/include")
ENDIF(WIN32)	
