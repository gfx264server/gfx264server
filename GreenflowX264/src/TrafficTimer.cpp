/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "./TrafficTimer.h"
#include "./Logger.h"

#include <boost/bind.hpp>

TrafficTimer::TrafficTimer()
: mTimer(0)
{
	//Empty
}

TrafficTimer::~TrafficTimer()
{
	//Empty
}

// ----------------------------------------------------------------------------
// ------------------------- Public -------------------------------------------
// ----------------------------------------------------------------------------

void TrafficTimer::setTimer(boost::asio::deadline_timer *timer){
	mTimer = timer;
}

void TrafficTimer::expiresFromNow(int seconds){
	mTimer->expires_from_now(boost::posix_time::seconds(seconds));
	mTimer->async_wait(boost::bind(&TrafficTimer::onTimeout,this,_1));
}

void TrafficTimer::cancel(void){
	mTimer->cancel();
}

// ----------------------------------------------------------------------------
// ------------------------- Private ------------------------------------------
// ----------------------------------------------------------------------------

void TrafficTimer::onTimeout(const boost::system::error_code &e){
	if (e != boost::asio::error::operation_aborted){
		//Logger::log(__PRETTY_FUNCTION__,"Timer");
		expiresFromNow(180);
		timeoutSig();
	}
	if (e == boost::asio::error::operation_aborted){
		//Logger::log(__PRETTY_FUNCTION__,"ABORTED");
		; //std::cerr << "----------TIMERTIMERTIMER-----------" << e << std::endl;
	}
}

