/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "./RawCluster.h"

#ifdef __unix
#include <arpa/inet.h>
#endif
#ifdef _WIN32
#include <winsock2.h>
#endif

#include <vector>

RawCluster::RawCluster()
{
	//Empty
}

RawCluster::~RawCluster()
{
	//Empty
}

// ----------------------------------------------------------------------------
// ------------------------- Public -------------------------------------------
// ----------------------------------------------------------------------------

void RawCluster::addData(char *data, int size, int type){
	if(size <= 0){
		return;
	}

	RawCluster::frame cluster(data, data+size);
	cluster.push_back(htons(type) & 0xFF);
	cluster.push_back(htons(type) >> 8);

	mCluster.push_back(cluster);
}

std::vector<RawCluster::frame> *RawCluster::getCluster(void){
	return &mCluster;
}

int RawCluster::frames(void) const{
	return mCluster.size();
}
