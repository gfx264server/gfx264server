/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <memory>
#include <vector>
#include <stdint.h>
#include <boost/shared_ptr.hpp>

/**
	@class Image
	@brief Contains data of I402 encoded image.
	This class is used to transfer image data from sender to X264Server.
	It contains a readily I402 encoded image, its size, and the time how long
	the image is to be shown.
*/
class Image
{
	
	typedef boost::shared_ptr<std::vector<char> > imagePtr;
	
public:

	Image(imagePtr image, int width, int height);

	~Image();

	uint8_t *data(void);

	int width(void) const;

	int height(void) const;

	void setSecondsToShow(unsigned int seconds);

	unsigned int secondsToShow(void) const;

	void setSequence(bool sequence);

	bool isSequence(void) const;

	void setLastFrame(bool lastFrame);

	bool isLastFrame(void) const;

	int size();

private:

	// --- Initialised in Constructor initialisation list -----------------------
	imagePtr     mImage;
	const int    mWidth;
	const int    mHeight;
	unsigned int mSecondsToShow;
	bool         mIsSequence;
	bool         mLastFrame;
	// --------------------------------------------------------------------------

};

#endif 
