/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _X264STATUS_H_
#define _X264STATUS_H_

#include <boost/asio.hpp>
using boost::asio::ip::udp;

/**
	@class X264Status
	@brief Rudientary state machine.
	Holds a couple of states, in which the server can be, e.h. whether or
	not receiving of new image data is allowed, whether or not an encoder is
	created, etc.
*/
class X264Status
{
public:

	enum X264StatusEnum{
			eUndefinedStatus
		, eReceiving
		, eSending
		, eInitialized
		, eEncoderCreated
		, eDoneReceiving
		, eStartSending
	};

	X264Status();

	~X264Status();

	void setInitialized(const boost::asio::ip::address &address, int port);

	void encoderCreated(void);

	bool encoderCreationOk(void) const;

	bool receivingOk(void) const;

	bool isReceiving(void) const;

	void setDoneReceiving(void);

	void setStartSending(void);

	void setReceiving(void);

	bool isEncoderCreated(void) const;

private:

	X264StatusEnum               mStatus;
	boost::asio::ip::address     mAddress;
	int                          mPort;

};

#endif
