/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "./StringFunctions.h"
#include "src/GreenflowX264Config.h"

#include <iomanip>
#include <sstream>
#include <boost/crc.hpp>

std::auto_ptr<std::vector<unsigned char> > 
StringFunctions::fromHex(const std::string &hexstr){
	std::auto_ptr<std::vector<unsigned char> >  bytes = 
		std::auto_ptr<std::vector<unsigned char> >(new  std::vector<unsigned char>());
	for(std::string::size_type i = 0; i < hexstr.size() / 2; ++i){
		std::istringstream iss(hexstr.substr(i * 2, 2));
		unsigned int n;
		iss >> std::hex >> n;
		bytes->push_back(static_cast<unsigned char>(n));
	}
	return bytes;
}

std::string StringFunctions::toHexString(int value){
	std::stringstream stream;
	stream << std::setfill ('0')  << std::setw(4) << std::uppercase << std::hex << value;
	std::string result( stream.str());
	return result;
}
	
boost::uint16_t StringFunctions::extractSequenceNumber(const std::vector<char> &payload){
	boost::uint16_t t = (payload[0] << 8) | (payload[1] & 0xFF);
	return t;
}

std::vector<unsigned char> *StringFunctions::magic(void){
	return fromHex(GreenflowMagic).release();
}

int StringFunctions::getCRC32(const std::vector<unsigned char> &payload){
	boost::crc_ccitt_type result;
	result.process_bytes(payload.data(), payload.size());	
	return result.checksum();
}

int StringFunctions::getCRC32(const std::vector<char> &payload){
	boost::crc_ccitt_type result;
	result.process_bytes(payload.data(), payload.size());
	return result.checksum();
}

