/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _IMAGERECEIVER_H_
#define _IMAGERECEIVER_H_

#include "../../CommonCode/Commands.h"

#include <vector>
#include <map>
#include <queue>
#include <boost/signals2.hpp>
#include <sstream>
class X264Base;
class X264Status;
class ResponseBuffer;
class CommandSet;
class TrafficTimer;
class Image;

	namespace s = boost::signals2;

/**
	@class ImageReceiver
	@brief Recveived package data. Feeds data to X264 and returns result frames.
	Gets data from PackageHandler. As soon as a received Image is complete,
	the data is fed into the X264Base. X264Base announces a complete run via
	signal. The complete data is send back to the GF client.
*/
class ImageReceiver
{

public:

	typedef std::vector<unsigned char> dataVector;
	typedef std::pair<unsigned int, std::vector<unsigned char> > element;
	typedef std::map<unsigned int, std::vector<unsigned char> >  dataMap;
	
	ImageReceiver(const std::vector<unsigned char> &payload);

	~ImageReceiver();
	
	/**
		Receives an Image part from PackageHandler. Checks if sequence number is
		ok and adds it to the buffer. Requests for next package or asks to
		repeat missing/corrupted.
	*/
	void package(unsigned int seqNo, const dataVector &payload);
	
	void stopConfirmed();

	/**
		Give ImageReceiver a pointer to the global X264Status object. Used to
		set and query certain modes the X264Server can be in.
	*/
	void setX264Status(X264Status *x264Status);

	/**
		Give ImageReceiver a pointer to global ResponseBuffer. Allows to send
		packages directly to GF client.
	*/
	void setResponseBuffer(ResponseBuffer *buffer);

	/**
		Give ImageReceiver a pointer to global CommandSet object. Allows to do
		basic bookkeeping on sent and received commands.
	*/
	void setCommandSet(CommandSet *commandSet);

	/**
		The transfer of an image from a client is completed. Process the received
		data and feed the image to the X264 lib.
	*/
	void done(void);

	void createEncoder(int width, int height, const std::string &profile, int fps);

	void sendEncodedBlock(int index);

	void setHeaderSize(int size);

	void doneReceive(void);

	void setFps(double fps);

	void closeEncoder(void);

	void sendOpen(void);

private:

	// --- Initialised in Constructor initialisation list -----------------------
	TrafficTimer       * const mTrafficTimer;
	X264Base           *       mX264Base;
	unsigned int               mSequenceNo;
	int                        mOffset;
	bool                       mError;
	bool                       mDone;
	int                        mHeaderSize;
	int                        mPayloadSize;
	ResponseBuffer     *       mResponseBuffer;
	CommandSet         *       mCommandSet;
	X264Status         *       mX264Status;
	double                     mFps;
	int                        mSendIndex;
	int                        mSerial;
	int                        mClose;
	// --------------------------------------------------------------------------

	dataMap                                 mImageDataMap;
	std::vector<char>                       mPayload;
	std::vector<std::vector<char> >        *mCluster;
	std::ostringstream                      mOut;
	std::queue<boost::shared_ptr<Image> >   mSequenceImages;

	static const unsigned int  BLOCKSIZE = 30000;

	void createX264Base(void);

	void storeImage(boost::shared_ptr<Image> image);

	/**
		sequenceCorruption only sets the mError variable. When the client has
		sent all its packages notified the end of the transfer, this mError
		is checked. If true, the missing packages are requested again.
	*/
	void sequenceCorruption(unsigned int seqNo);

	/**
		If mError is set, the mImageDataMap is checked for missing entries.
		TODO: Request missing.
	*/
	void fixErrors(void);

	void sendCluster(void);

	void errorSlt(GreenflowErrors::Error error);

	void frameDoneSlt(void);

	/**
		Sends GreenflowX264Commands::startSending to client.
		Client starts sending with sequence number 0
	*/
	void sendStart(void);

	/**
		Sends GreenflowX264Commands::stopSending to client.
		Client confirms with GreenflowX264Commands::stopSending. Used to reset
		the block index.
	*/
	void sendStop(void);

	/**
		Sends GreenflowX264Commands::nextSending to client.
		Request from client to send new package (datagram, part of an Image).
	*/
	void nextPackage(void);

	void timerSlt(void);

	void startEncodedFrame(void);

	void sendAV(const std::string &str);

	void requestPackage(unsigned int pack);
};

#endif 
