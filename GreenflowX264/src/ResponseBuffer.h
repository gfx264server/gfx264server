/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _RESPONSEBUFFER_H_
#define _RESPONSEBUFFER_H_

#include "../../CommonCode/Commands.h"
#include <vector>
#include <boost/asio.hpp>
using boost::asio::ip::udp;
using boost::asio::io_service;

/**
	@class ResponseBuffer
	@brief Contains methods to send data to GF receiver.
*/
class ResponseBuffer
{
public:

	typedef std::vector<unsigned char> responseBuffer;

	ResponseBuffer(udp::socket &socket, udp::endpoint &endpoint);

	void setCommand(const char &command);

	/**
		Appends a single uint16 value to the payload responseBuffer. The uint16
		value is converted from host to network byte order.
	*/
	void appendInt16(boost::uint16_t value);

	void appendString(const std::string &str);

	const responseBuffer *magic(void) const;

	void sendData(void);

	boost::asio::ip::address address(void) const;

	unsigned short port(void) const;

	std::size_t headerSize(void) const;

	bool magicOk(const std::vector<unsigned char> &buffer) const;

	void sendMessage(GreenflowX264Commands::Commands command);

	void sendErrorMessage(GreenflowErrors::Error error, 
		const std::string &str = "");

	void clear(void);

private:

	// --- Initialised in Constructor initialisation list -----------------------
	responseBuffer const * const mMagic;
	udp::socket                 &mSocket;
	udp::endpoint               &mRemoteEndpoint;
	// --------------------------------------------------------------------------

	responseBuffer               mResponse;
	std::size_t                  mHeaderSize;

};

#endif
