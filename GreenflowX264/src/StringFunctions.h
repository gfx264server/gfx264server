/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef _STRINGFUNCTIONS_H_
#define _STRINGFUNCTIONS_H_

#include <memory>
#include <vector>
#include <string>
#include <boost/cstdint.hpp>

/**
	@namespace StringFunctions
	@brief A couple of widely used funktions to convert strings and vector<char>.
*/
namespace StringFunctions
{
	
	std::auto_ptr<std::vector<unsigned char> > 
		fromHex(const std::string &hexstr);
	
	std::string toHexString(int value);
	
	boost::uint16_t extractSequenceNumber(const std::vector<char> &payload);

	std::vector<unsigned char> *magic(void);
	
	int getCRC32(const std::vector<unsigned char> &payload);
	
	int getCRC32(const std::vector<char> &payload);

};

#endif 
