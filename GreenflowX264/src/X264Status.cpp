/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "./X264Status.h"

X264Status::X264Status()
: mStatus(eUndefinedStatus)
{
	//Empty
}


X264Status::~X264Status()
{
	//Empty
}

// ----------------------------------------------------------------------------
// ------------------------- Public -------------------------------------------
// ----------------------------------------------------------------------------

void X264Status::setInitialized(const boost::asio::ip::address &address,
int port){
	mStatus  = eInitialized;
	mAddress = address;
	mPort    = port;
}

void X264Status::encoderCreated(void){
	mStatus = eEncoderCreated;
}

bool X264Status::isEncoderCreated(void) const{
	return mStatus == eEncoderCreated;
}

bool X264Status::encoderCreationOk(void) const{
	return mStatus == eInitialized;
}

bool X264Status::receivingOk(void) const{
	return mStatus == eEncoderCreated || mStatus == eReceiving;
}

void X264Status::setDoneReceiving(void){
	mStatus = eDoneReceiving;
}

void X264Status::setStartSending(void){
	mStatus = eStartSending;
}

void X264Status::setReceiving(void){
	mStatus = eReceiving;
}

bool X264Status::isReceiving(void) const{
	return mStatus == eReceiving;
}




