/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _PACKAGEHANDLER_H_
#define _PACKAGEHANDLER_H_

#include "../../CommonCode/Commands.h"
class ImageReceiver;
class CommandSet;
class ResponseBuffer;
class X264Status;
class TrafficTimer;

#include <vector>
#include <map>

/**
	@class PackageHandler
	@brief Highlevel package dispatcher
	Receives formally correct packages from UdpServer, analyses the content
	and sends the package to the appropriate receive. Currently the only
	receiver is the ImageReceiver.
*/
class PackageHandler
{

public:

	PackageHandler(ResponseBuffer * const response);

	~PackageHandler();

	/**
		Receives data from UdpServer. Evaluates the command and executest the
		appropriate handler.
	*/
	void process(int command, int sequenceNo, 
		const std::vector<unsigned char> &payload);

private:

	typedef void (PackageHandler::*simpleFunc)();

	typedef void (PackageHandler::*paramFunc)
		(int, const std::vector<unsigned char> &);

	// --- Initialised in Constructor initialisation list -----------------------
	TrafficTimer      * const mTrafficTimer;
	ResponseBuffer    * const mResponseBuffer;
	CommandSet        * const mCommandSet;
	X264Status        * const mX264Status;
	ImageReceiver     *       mImageReceiver;
	// --------------------------------------------------------------------------

	std::map<GreenflowX264Commands::Commands, simpleFunc> simpleCommand;

	std::map<GreenflowX264Commands::Commands, paramFunc> paramCommand;

	void replyInit(void);

	void stopSending(void);

	/**
		Sent by client, when last part of an Image is transferred.
	*/
	void doneSending(void);

	void doneReceive(void);

	void nextEncodedBlock(void);

	void startEncodedFrame(void);

	void killServer(void);

	void closeEncoder(void);

	void sendMessage(GreenflowX264Commands::Commands command);

	void createEncoder(int seqNo, const std::vector<unsigned char> &payload);

	void sendFrame(int seqNo, const std::vector<unsigned char> &payload);

};

#endif 
