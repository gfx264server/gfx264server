/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef _COMMANDSET_H_
#define _COMMANDSET_H_

#include "../../CommonCode/Commands.h"

#include <set>

/**
	@class CommandSet
	@brief Bookkeeping of send and received commands.
	Allows to make sure that a received answer was actually asked.
	Part of the safeguard against the inherently unreliable UDP protocol.
*/
class CommandSet
{

public:

	CommandSet();

	~CommandSet();
	
	void addCommand(GreenflowX264Commands::Commands command);
	
	void removeCommand(GreenflowX264Commands::Commands command);
	
	bool containsCommand(GreenflowX264Commands::Commands command);
	
private:
	
	std::set<GreenflowX264Commands::Commands>   mCommandSet;
};

#endif 
