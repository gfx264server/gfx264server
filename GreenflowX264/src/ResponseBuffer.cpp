/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "./ResponseBuffer.h"
#include "./StringFunctions.h"
#include <iostream>
using boost::asio::ip::address_v4;
using boost::asio::buffer;
typedef boost::asio::ip::udp::endpoint endpoint;

ResponseBuffer::ResponseBuffer(udp::socket &socket, udp::endpoint &endpoint)
: mMagic(StringFunctions::magic())
, mSocket(socket)
, mRemoteEndpoint(endpoint)
{
	mResponse.insert(mResponse.begin(), mMagic->begin(), mMagic->end());
	mResponse.push_back('\0');
	mResponse.push_back('\0');
	mResponse.push_back('\0');
	mResponse.push_back('\0');
	mResponse.push_back('\0');
	mResponse.push_back('\1');
	mResponse.push_back('\1');
	mHeaderSize = mResponse.size();
}

const ResponseBuffer::responseBuffer *ResponseBuffer::magic(void) const{
	return mMagic;
}

bool ResponseBuffer::magicOk(const std::vector<unsigned char> &buffer) const{
	std::vector<unsigned char> hash(buffer.begin(), 
		buffer.begin()+mMagic->size());
	return (hash == *mMagic);
}

void ResponseBuffer::setCommand(const char &command){
	mResponse[GreenflowX264Commands::COMMAND] = command;
}


void ResponseBuffer::appendInt16(boost::uint16_t value){
	mResponse.push_back(htons(value)  & 0xFF);
	mResponse.push_back(htons(value)  >> 8);
}

void ResponseBuffer::appendString(const std::string &str){
	mResponse.insert(mResponse.end(), str.begin(),str.end());
}

void ResponseBuffer::sendData(void){
	endpoint destination(address(), port());
	boost::system::error_code ec;
	mSocket.send_to(buffer(mResponse), destination,0,ec);
	if(ec){
		//std::cerr << "Send Error:" << ec << std::endl;
	}
	mResponse.resize(mHeaderSize);
}

boost::asio::ip::address ResponseBuffer::address(void) const{
	return mRemoteEndpoint.address();
}

unsigned short ResponseBuffer::port(void) const{
	return mRemoteEndpoint.port();
}

std::size_t ResponseBuffer::headerSize(void) const{
	return mHeaderSize;
}

void ResponseBuffer::sendMessage(GreenflowX264Commands::Commands command){
	setCommand(static_cast<char>(command));
	sendData();
}

void ResponseBuffer::sendErrorMessage(GreenflowErrors::Error error,
const std::string &str){
	setCommand(GreenflowX264Commands::errorMessage);
	appendInt16(error);
	appendString(str);
	sendData();
}

void ResponseBuffer::clear(void){
	mResponse.resize(mHeaderSize);	
}
