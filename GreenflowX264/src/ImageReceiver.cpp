/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "./ImageReceiver.h"
#include "./X264Status.h"
#include "./ResponseBuffer.h"
#include "./X264Exception.h"
#include "./CommandSet.h"
#include "./TrafficTimerSingleton.h"
#include "./StringFunctions.h"
#include "./X264Code/X264Base.h"
#include "./Logger.h"

#include <iostream>
#include <sstream>
#include <memory>
#include <fstream>

#include <boost/shared_ptr.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/archive/binary_oarchive.hpp>

using boost::iostreams::stream;
using boost::iostreams::array_source;

//BEGIN Constructor/Destructor
ImageReceiver::ImageReceiver(const std::vector<unsigned char> &payload)
: mTrafficTimer(TrafficTimerSingleton::instance())
, mX264Base(0)
, mSequenceNo(0)
, mOffset(0)
, mError(false)
, mDone(false)
, mHeaderSize(-1)
, mPayloadSize(-1)
, mResponseBuffer(0)
, mCommandSet(0)
, mX264Status(0)
, mFps(8.0)
, mSendIndex(0)
{
	mImageDataMap.insert(element(0,payload));
	createX264Base();

	mTrafficTimer->timeoutSig.connect(
		boost::bind(&ImageReceiver::timerSlt,this));
}

ImageReceiver::~ImageReceiver()
{
	//Empty
}
//END

// ----------------------------------------------------------------------------
// ------------------------- Public -------------------------------------------
// ----------------------------------------------------------------------------

void ImageReceiver::setX264Status(X264Status *x264Status){
	assert(x264Status && "No 0 Pointer here");
	assert(!mX264Status && "No reassignment");
	mX264Status = x264Status;
}

void ImageReceiver::setResponseBuffer(ResponseBuffer *buffer){
	assert(buffer && "No 0 Pointer here");
	assert(!mResponseBuffer && "No reassignment");
	mResponseBuffer = buffer;
}

void ImageReceiver::setCommandSet(CommandSet *commandSet){
	assert(commandSet && "No 0 Pointer here");
	assert(!mCommandSet && "No reassignment");
	mCommandSet = commandSet;
}

void ImageReceiver::package(unsigned int seqNo,
const std::vector<unsigned char> &payload){
	if(seqNo != mSequenceNo){
		sequenceCorruption(seqNo);
		mSequenceNo = seqNo;
	}
	mImageDataMap.insert(element(seqNo+mOffset,payload));
	if(mSequenceNo == BLOCKSIZE){
		mSequenceNo = 0;
		mOffset += BLOCKSIZE+1;
		sendStop();
	}else{
		if(mDone){
			return;
		}
		++mSequenceNo;
		nextPackage();
	}
}

void ImageReceiver::stopConfirmed(){
	mSequenceNo = 0;
	sendStart();
}

void ImageReceiver::done(void){
	if(mError){
		fixErrors();
	}
	mDone = true;
	mX264Status->setDoneReceiving();
	dataMap::iterator it;
	mPayload.clear();
	for(it = mImageDataMap.begin(); it != mImageDataMap.end(); ++it){
		mPayload.insert(mPayload.end(), it->second.begin(),it->second.end());
	}
	stream<array_source> in1(mPayload.data(), mPayload.size());
	boost::iostreams::filtering_streambuf<boost::iostreams::input> in;
	try{
		boost::iostreams::zlib_params deflate_param;
		deflate_param.noheader = true;
		in.push(boost::iostreams::zlib_decompressor(deflate_param));
		in.push(in1);

		std::vector<char> output_buffer2;
		boost::iostreams::copy(in, std::back_inserter(output_buffer2));

		int width        = ntohs(output_buffer2[0] + (output_buffer2[1] << 8));
		int height       = ntohs(output_buffer2[2] + (output_buffer2[3] << 8));
		int secsToShow   = ntohs(output_buffer2[4] + (output_buffer2[5] << 8));
		mSerial          = ntohs(output_buffer2[6] + (output_buffer2[7] << 8));
		bool isSequence  = (output_buffer2[8] == '1');
		bool isLastFrame = (output_buffer2[9] == '1');
		boost::shared_ptr<std::vector<char> >
			ptr(new std::vector<char>(output_buffer2.begin()+10
		,
			output_buffer2.end()));
		boost::shared_ptr<Image> i(new Image(ptr, width, height));
		i->setLastFrame(isLastFrame);
		i->setSecondsToShow(secsToShow);
		i->setLastFrame(isLastFrame);
		if(isSequence){
			storeImage(i);
		}else{
			mTrafficTimer->expiresFromNow(180);
			mX264Base->cluster(i, mFps);
		}
	}catch(const std::exception &bla){
		(void) bla;
		//std::cout << "-----------------------" << bla.what() << std::endl;
	}
}

void ImageReceiver::createEncoder(int width, int height, 
const std::string &profile, int fps){
	mTrafficTimer->expiresFromNow(180);
	mX264Base->setProfile(profile,fps);
	std::vector<char> *d = mX264Base->streamingHeader(width,height);
	std::ostringstream out(std::ios::out|std::ios::binary);
	boost::archive::text_oarchive oa(out);
	oa << *d;
	std::string av = out.str();
	mImageDataMap.clear();
	mX264Status->encoderCreated();
	sendAV(av);
}

void ImageReceiver::sendCluster(void){
	boost::archive::binary_oarchive oa(mOut);
	oa << mCluster;
	startEncodedFrame();
	mSendIndex = 0;
}

void ImageReceiver::sendEncodedBlock(int index){
	(void) index;
	mTrafficTimer->cancel();
	unsigned int pSize = mPayloadSize - 2;
	std::string data(mOut.str(),mSendIndex*pSize,pSize);
	if(mSendIndex*pSize >= mOut.str().size()-pSize){
		mX264Status->setReceiving();
		mSequenceNo = 0;
		mDone = false;
		mOut.clear();
		mOut.str("");
		mOffset = 0;
		mImageDataMap.clear();
		mResponseBuffer->setCommand(GreenflowX264Commands::sendLastEncodedBlock|128);
		mResponseBuffer->appendInt16(mSerial);
		std::ofstream outputFile;
		outputFile.open("/tmp/GreenflowX264Log.log", 
			std::ofstream::out | std::ofstream::app);
		outputFile << "LAST ENCODED BLOCK" << std::endl;
		outputFile.close();
	}else{
		mResponseBuffer->setCommand(GreenflowX264Commands::sendEncodedBlock|128);
	}
	mResponseBuffer->appendInt16(mSendIndex);
	mResponseBuffer->appendString(data);
	mResponseBuffer->sendData();
	if(mSendIndex*pSize >= mOut.str().size()-pSize){
		mSendIndex = 0;
	}else{
		++mSendIndex;
	}
}

void ImageReceiver::setHeaderSize(int size){
	mHeaderSize = size;
	mPayloadSize = GreenflowX264Commands::sendBufferSize() - size;
}

void ImageReceiver::doneReceive(void){

}

void ImageReceiver::setFps(double fps){
	mFps = fps;
}

void ImageReceiver::closeEncoder(void){
	mSequenceNo = 0;
	mOffset     = 0;
	mError      = false;
	mDone       = false;
	mSendIndex  = 0;
	mImageDataMap.clear();
	mPayload.clear();
	mClose = mSerial;
	mOut.clear();
	mOut.str("");
	std::queue<boost::shared_ptr<Image> > empty;
	std::swap(mSequenceImages, empty);
	mX264Base->closeEncoder();
	mResponseBuffer->clear();
	mX264Base->openEncoder();
	sendOpen();
	mX264Status->setReceiving();
}

// ----------------------------------------------------------------------------
// ------------------------- Private ------------------------------------------
// ----------------------------------------------------------------------------

void ImageReceiver::createX264Base(void){
	delete(mX264Base);
	mX264Base = new X264Base;
	mX264Base->errorSig.connect(boost::bind(&ImageReceiver::errorSlt,this,_1));
	mX264Base->doneSig.connect(boost::bind(&ImageReceiver::frameDoneSlt,this));
}

void ImageReceiver::sequenceCorruption(unsigned int seqNo){
	(void) seqNo;
	//TODO: message to client.
	//std::cerr << "NO SEQUENTIAL-----" << std::dec << seqNo
	//	<< " --- Expected: " << mSequenceNo << std::endl;
	mError = true;
}

void ImageReceiver::fixErrors(void){
	dataMap::iterator it;
	for(unsigned int i = 0; i < mImageDataMap.size(); ++i){
		if(mImageDataMap.find(i) == mImageDataMap.end()){
			//std::cerr << "Missing: " << std::dec << i << std::endl;
		}
	}
}

void ImageReceiver::sendStop(void){
	char command = GreenflowX264Commands::stopSending|128;
	mResponseBuffer->setCommand(command);
	mCommandSet->addCommand(GreenflowX264Commands::stopSending);
	mResponseBuffer->sendData();
}

void ImageReceiver::sendOpen(void){
	mSequenceNo = 0;
	mOffset     = 0;
	mError      = false;
	mDone       = false;
	mSendIndex  = 0;
	mImageDataMap.clear();
	mPayload.clear();
	std::queue<boost::shared_ptr<Image> > empty;
	std::swap(mSequenceImages, empty);
	mResponseBuffer->clear();
	char command = GreenflowX264Commands::sendOpen|128;
	mResponseBuffer->setCommand(command);
	mResponseBuffer->sendData();
}

void ImageReceiver::nextPackage(void){
	char command = GreenflowX264Commands::nextSending|128;
	mResponseBuffer->setCommand(command);
	//mTrafficTimer->expiresFromNow(5);
	mResponseBuffer->appendInt16(10);
	mResponseBuffer->sendData();
}

void ImageReceiver::sendStart(void){
	char command = GreenflowX264Commands::startSending|128;
	mResponseBuffer->setCommand(command);
	mCommandSet->addCommand(GreenflowX264Commands::startSending);
	mResponseBuffer->sendData();
}

void ImageReceiver::startEncodedFrame(void){
	mX264Status->setReceiving();
	//mTrafficTimer->cancel();
	mCommandSet->addCommand(GreenflowX264Commands::startEncodedFrame);
	char command = GreenflowX264Commands::startEncodedFrame|128;
	mResponseBuffer->setCommand(command);
	mResponseBuffer->sendData();
}

void ImageReceiver::sendAV(const std::string &str){
	char command = GreenflowX264Commands::encoderReply;
	mResponseBuffer->setCommand(command);
	mResponseBuffer->appendString(str);
	mResponseBuffer->sendData();
}

void ImageReceiver::requestPackage(unsigned int pack){
	mCommandSet->addCommand(GreenflowX264Commands::requestMissing);
	char command = GreenflowX264Commands::requestMissing|128;
	mResponseBuffer->setCommand(command);
	mResponseBuffer->appendInt16(pack);
	mResponseBuffer->sendData();
}

void ImageReceiver::storeImage(boost::shared_ptr<Image> image){
	mSequenceImages.push(image);
	if(image->isLastFrame()){
		mTrafficTimer->expiresFromNow(180);
		mX264Base->createTransition(mSequenceImages, mFps);
	}else{
		mX264Status->setReceiving();
		mSequenceNo = 0;
		mDone       = false;
		mOut.clear();
		mOut.str("");
		mOffset = 0;
		mImageDataMap.clear();
		char command = GreenflowX264Commands::sendNextImage|128;
		mResponseBuffer->setCommand(command);
		mResponseBuffer->sendData();
	}
}

// ----------------------------------------------------------------------------
// ------------------------- Private Slots ------------------------------------
// ----------------------------------------------------------------------------

void ImageReceiver::errorSlt(GreenflowErrors::Error error){
	switch(error){
		case GreenflowErrors::eDurationError:
		break;
		case GreenflowErrors::eEmptyFrameError:
		break;
		default:
			; //std::cerr << "Undefined Error" << std::endl;
	}
}

void ImageReceiver::frameDoneSlt(void){
	try{
		mCluster = mX264Base->getCluster();
		std::queue<boost::shared_ptr<Image> > empty;
		std::swap(mSequenceImages, empty);
		sendCluster();
	}catch(const X264Exception &error){
		//Send error!
		//std::cerr << ">>>>X264EXCEPTION<<<<" << std::endl;
	}
}

void ImageReceiver::timerSlt(void){
	//Logger::log(__PRETTY_FUNCTION__,"timer");
	return;
	if(mX264Status->isReceiving()){
		nextPackage();
	}else{
		if(mX264Status->isEncoderCreated()){
			nextPackage();
			return;
		}
		mResponseBuffer->sendMessage(GreenflowX264Commands::lostPackage);
	}
}

