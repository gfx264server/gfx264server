/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef _UDPSERVER_H_
#define _UDPSERVER_H_

class PackageHandler;
class ResponseBuffer;
class TrafficTimer;

#include <boost/asio.hpp>

using boost::asio::ip::udp;
using boost::asio::io_service;

/**
	@class UdpServer
	@brief Listener for incoming UDP packages.
	This class contains the UDP socket, which receives messages from the GF
	image sender. Incoming packages pass through a simple consistancy check
	and then, if the packages are formally valid, are passed to the
	PackageHandler.
*/
class UdpServer{

public:

	UdpServer(int port, const std::string &addr, io_service &io_service);
	
	~UdpServer();

private:

	// --- Initialised in Constructor initialisation list -----------------------
	boost::asio::strand                    mStrand;
	udp::socket                            mSocket;
	boost::asio::deadline_timer            mTimer;

	ResponseBuffer                 * const mResponseBuffer;
	PackageHandler                 * const mPackageHandler;
	TrafficTimer                   * const mTrafficTimer;
	std::vector<unsigned char>             mRecvBuffer;
	udp::endpoint                          mRemoteEndpoint;
	// --------------------------------------------------------------------------

	void startReceive();

	void handleReceive(const boost::system::error_code &error,
		std::size_t bytes_transferred);

	boost::uint16_t sequenceNo(void) const;

	void removeHeader(std::size_t bytes_transferred);

	bool packageOk(std::size_t bytes_transferred) const;

};

#endif
