/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef _X264EXCEPTION_H_
#define _X264EXCEPTION_H_

#include "../../CommonCode/Commands.h"

/**
	@class X264Exception
	@brief Encapsulates all exceptions, which X264Server objects know.
*/
class X264Exception
{

public:

	X264Exception();

	~X264Exception();

	void setError(GreenflowErrors::Error error);

	GreenflowErrors::Error error(void) const;

private:

	// --- Initialised in Constructor initialisation list -----------------------
	GreenflowErrors::Error     mError;
	// --------------------------------------------------------------------------

};

#endif
