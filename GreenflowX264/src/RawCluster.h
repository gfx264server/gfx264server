/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _RAWCLUSTER_H_
#define _RAWCLUSTER_H_

#include <vector>

/**
	@class RawCluster
	@brief Collects frames in a vector< frame > structure
	Collects single frames and stores them as a vector< frame >
	This form is used to be sent to the GF receiver via UDP.
*/
class RawCluster
{

	typedef std::vector<char> frame;

public:

	RawCluster();

	~RawCluster();

	/**
		Add a new frame to mCluster.
		@param data C-style array to frame data
		@param size size of data
		@param type frame type
	*/
	void addData(char *data, int size, int type);
	
	/**
		Return total number of currently hold frames.
	*/
	int frames(void) const;
	
	/**
		Return pointer to vector of frames.
	*/
	std::vector<frame> *getCluster(void);

private:

	std::vector<frame>  mCluster;

};

#endif
