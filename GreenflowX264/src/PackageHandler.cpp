/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "./PackageHandler.h"
#include "./ImageReceiver.h"
#include "./ResponseBuffer.h"
#include "./CommandSet.h"
#include "./X264Status.h"
#include "./Image.h"
#include "./TrafficTimerSingleton.h"
#include "src/GreenflowX264Config.h"
#include "../../CommonCode/Commands.h"


#include <fstream>

#include <cassert>
#include <iostream>
#include <sstream>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/string.hpp>

//BEGIN Constructor/Destructor
PackageHandler::PackageHandler(ResponseBuffer * const response)
: mTrafficTimer(TrafficTimerSingleton::instance())
, mResponseBuffer(response)
, mCommandSet(new CommandSet())
, mX264Status(new X264Status())
, mImageReceiver(0)
{
	#ifdef __unix
	signal(SIGPIPE, SIG_IGN);
	#endif
	// Simple commands without parameters.
	simpleCommand.insert(std::pair<GreenflowX264Commands::Commands, simpleFunc>(
		GreenflowX264Commands::initConnection, &PackageHandler::replyInit));
	simpleCommand.insert(std::pair<GreenflowX264Commands::Commands, simpleFunc>(
		GreenflowX264Commands::stopSending, &PackageHandler::stopSending));
	simpleCommand.insert(std::pair<GreenflowX264Commands::Commands, simpleFunc>(
		GreenflowX264Commands::doneSending, &PackageHandler::doneSending));
	simpleCommand.insert(std::pair<GreenflowX264Commands::Commands, simpleFunc>(
		GreenflowX264Commands::doneReceive, &PackageHandler::doneReceive));
	simpleCommand.insert(std::pair<GreenflowX264Commands::Commands, simpleFunc>(
		GreenflowX264Commands::startEncodedFrame,
		&PackageHandler::startEncodedFrame));
	simpleCommand.insert(std::pair<GreenflowX264Commands::Commands, simpleFunc>(
		GreenflowX264Commands::nextEncodedBlock,
		&PackageHandler::nextEncodedBlock));
	simpleCommand.insert(std::pair<GreenflowX264Commands::Commands, simpleFunc>(
		GreenflowX264Commands::killServer,
		&PackageHandler::killServer));
	simpleCommand.insert(std::pair<GreenflowX264Commands::Commands, simpleFunc>(
		GreenflowX264Commands::closeEncoder,
		&PackageHandler::closeEncoder));

	// Commands, which need sequenceNo and/or payload.
	paramCommand.insert(std::pair<GreenflowX264Commands::Commands, paramFunc>(
		GreenflowX264Commands::createEncoder, &PackageHandler::createEncoder));
	paramCommand.insert(std::pair<GreenflowX264Commands::Commands, paramFunc>(
		GreenflowX264Commands::sendFrame, &PackageHandler::sendFrame));

}

PackageHandler::~PackageHandler()
{
	delete(mCommandSet);
	delete(mX264Status);
	delete(mImageReceiver);
}
//END

// ----------------------------------------------------------------------------
// ------------------------- Public -------------------------------------------
// ----------------------------------------------------------------------------

//BEGIN process
void PackageHandler::process(int command, int sequenceNo, 
const std::vector<unsigned char> &payload){
	//std::cerr << "PROCESS " << command << std::endl;
	GreenflowX264Commands::Commands highBitCommand =
		static_cast<GreenflowX264Commands::Commands>(command);

	// Command from client can contain initial high bit. Get command without
	// this bit.
	GreenflowX264Commands::Commands greenflowCommand =
		static_cast<GreenflowX264Commands::Commands>(command&127);

	// Check if it is an initial command (hight bit set) or and answer. In that
	// case the command must be found in mCommandSet.
	if(!(highBitCommand & 128) &&
	   !mCommandSet->containsCommand(greenflowCommand)){
		//std::cerr << "Error, No high set" << std::endl;
		return;
	}
	
	if(paramCommand.count(greenflowCommand)){
		(*this.*paramCommand[greenflowCommand])(sequenceNo, payload);
		return;
	}

	if(simpleCommand.count(greenflowCommand)){
		(*this.*simpleCommand[greenflowCommand])();
		return;
	}

	mResponseBuffer->sendMessage(GreenflowX264Commands::unknownCommand);
}
//END

// ----------------------------------------------------------------------------
// ------------------------- Private ------------------------------------------
// ----------------------------------------------------------------------------

void PackageHandler::replyInit(void){
	char command = GreenflowX264Commands::initConnection|128;
	mResponseBuffer->setCommand(command);
	std::string version(GreenflowLib_Version);
	mResponseBuffer->appendString(version);
	mResponseBuffer->sendData();
	
	mX264Status->setInitialized(mResponseBuffer->address(),
		mResponseBuffer->port());
}

void PackageHandler::stopSending(void){
	mCommandSet->removeCommand(GreenflowX264Commands::stopSending);
	mImageReceiver->stopConfirmed();
}

void PackageHandler::doneSending(void){
	//mTrafficTimer->cancel();
	mImageReceiver->done();
}

void PackageHandler::doneReceive(void){
	mImageReceiver->doneReceive();
}

void PackageHandler::nextEncodedBlock(void){
	//std::cerr << "nextEncodedBlock" << std::endl;
	mImageReceiver->sendEncodedBlock(-1);
}

void PackageHandler::startEncodedFrame(void){
	mX264Status->setStartSending();
	mImageReceiver->sendEncodedBlock(0);
}

void PackageHandler::createEncoder(int seqNo,
const std::vector<unsigned char> &payload){
	//std::cerr << "CREATE ENCODER" << std::endl;
	(void) seqNo;
	if(!mX264Status->encoderCreationOk()){
		//std::cerr << "CreateEncoder failed" << std::endl;
		return;
	}
	std::string result(payload.begin(),payload.end());
	std::istringstream in(result);
	boost::archive::text_iarchive ia(in);
	int width              = 0;
	int height             = 0;
	double fps             = 0;
	std::string profile    = "";
	ia >> width;
	ia >> height;
	ia >> fps;
	ia >> profile;
	if(!mImageReceiver){
		mImageReceiver = new ImageReceiver(payload);
		mImageReceiver->setX264Status(mX264Status);
		mImageReceiver->setHeaderSize(mResponseBuffer->headerSize());
		mImageReceiver->setResponseBuffer(mResponseBuffer);
		mImageReceiver->setFps(fps);
		mImageReceiver->setCommandSet(mCommandSet);
	}
	mImageReceiver->createEncoder(width, height, profile, fps);
}

void PackageHandler::sendFrame(int seqNo,
const std::vector<unsigned char> &payload){
	//std::cerr << "SEND FRAME" << std::endl;
	if(!mX264Status->receivingOk()){
		//std::cerr << "receiving not ok" << std::endl;
		if(mImageReceiver){
			mImageReceiver->closeEncoder();
		}
		return;
	}
	if(seqNo == 0){
		if(mImageReceiver == 0){
			//std::cerr << "MISSING RECEIVER" << std::endl;
			; // Missing init
		}else{
			mImageReceiver->package(seqNo, payload);
		}
	}else{
		mImageReceiver->package(seqNo, payload);
	}
}

void PackageHandler::killServer(void){
	//std::cerr << "Kill Server" << std::endl;
	exit(1);
}

void PackageHandler::closeEncoder(void){
	std::ofstream outputFile;
	outputFile.open("/tmp/GreenflowX264Log.log", 
		std::ofstream::out | std::ofstream::app);
	outputFile << "CLOSE_ENCODER" << std::endl;
	outputFile.close();
	//std::cerr << "Close Encoder" << std::endl;
	mImageReceiver->closeEncoder();
}
