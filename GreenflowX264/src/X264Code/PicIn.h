/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _PICIN_H_
#define _PICIN_H_

#include "../Image.h"

#include <stdint.h>
#include <memory>
#include <vector>

extern "C"{
	#include "x264.h"
}

/**
	@class PicIn
	@brief Create x264 image structure.
	Takes a pointer to an Image and creates a x264_picture_t from it.
*/
class PicIn
{
	
	typedef boost::shared_ptr<Image> imagePtr;

public:
   
	PicIn(imagePtr image);

	~PicIn();

	x264_picture_t *picIn(void);

private:

	// --- Initialised in Constructor initialisation list -----------------------
	x264_picture_t *     mPicIn;
	Image          *     mI420Image;
	int                  mWidth;
	int                  mHeight;
	// --------------------------------------------------------------------------

};

#endif 
