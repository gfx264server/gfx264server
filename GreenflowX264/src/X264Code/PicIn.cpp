/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "./PicIn.h"
#include <memory.h>

PicIn::PicIn(imagePtr image)
: mPicIn(new x264_picture_t)
, mI420Image(image.get())
, mWidth(mI420Image->width())
, mHeight(mI420Image->height())
{
	x264_picture_alloc(mPicIn, X264_CSP_I420, mWidth, mHeight); // check < 0
	mPicIn->img.i_csp       = X264_CSP_I420;
	mPicIn->img.i_plane     = 3;
	mPicIn->img.i_stride[0] = mWidth;
	mPicIn->img.i_stride[1] = mWidth/2;
	mPicIn->img.i_stride[2] = mWidth/2;
	memcpy(mPicIn->img.plane[0], mI420Image->data(), image->size());
	mPicIn->img.plane[1]    = (mPicIn->img.plane[0]+mWidth*mHeight);
	mPicIn->img.plane[2]    = (mPicIn->img.plane[1]+mWidth*mHeight/4);
}

PicIn::~PicIn()
{
	x264_picture_clean(mPicIn);
	delete(mPicIn);
}

// ----------------------------------------------------------------------------
// ------------------------- Public -------------------------------------------
// ----------------------------------------------------------------------------

x264_picture_t *PicIn::picIn(void){
	return mPicIn;
}

