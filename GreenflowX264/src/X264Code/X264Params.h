/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef _X264PARAMS_H_
#define _X264PARAMS_H_

#include <stdint.h>
extern "C"{
	#include "x264.h"
}

/**
	@class X264Params
	@brief Parameter set for X264 creation
	Parameters to configure x264 encoder run. Currently only configurable
	parameters are the image size. X264 params are currently badly understood.
	Class will change as soon the meaning of most of the parameters become clear.
*/
class X264Params
{

public:

	X264Params(double fps);

	~X264Params();

	x264_param_t *param(void);

	void size(int width, int height);

	int width(void) const;

	int height(void) const;

private:

	// --- Initialised in Constructor initialisation list -----------------------
	x264_param_t *mParam;
	// --------------------------------------------------------------------------

};

#endif
