/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <boost/thread/thread.hpp>

#include "./X264Params.h"

X264Params::X264Params(double fps)
: mParam(new x264_param_t)
{
	x264_param_default_preset(mParam, "veryfast", "zerolatency");
	mParam->i_threads = boost::thread::hardware_concurrency();

	mParam->i_fps_num = fps * 100; 
	mParam->i_fps_den = 100;
	// Intra refres:
	//mParam->i_keyint_max = 25; //fps;
	mParam->b_intra_refresh = 1;
	//Rate control:
	mParam->rc.i_rc_method   = X264_RC_CRF;
	mParam->rc.f_rf_constant = 24;
	//mParam->rc.f_rf_constant_max = 35;
	//For streaming:
	mParam->b_repeat_headers = 0;
	mParam->b_annexb = 0;
}

X264Params::~X264Params()
{
	delete(mParam);
}

// ----------------------------------------------------------------------------
// ------------------------- Public -------------------------------------------
// ----------------------------------------------------------------------------

x264_param_t *X264Params::param(void){
	return mParam;
}

void X264Params::size(int width, int height){
	mParam->i_width   = width;
	mParam->i_height  = height;	
}

int X264Params::width(void) const{
	return mParam->i_width;
}

int X264Params::height(void) const{
	return mParam->i_height;
}

