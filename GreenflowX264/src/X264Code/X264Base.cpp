/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "./X264Base.h"

#include "./PicIn.h"
#include "./X264Params.h"
#include "../X264Exception.h"

#include <iostream>

#include <fstream>
//BEGIN Constructor/Destructor
X264Base::X264Base()
: mProfile("baseline")
, mEncoder(0)
, mFps(24.0)
, mCluster(0)
, mClusterReady(false)
, mNals(0)
, mINals(-1)
, mWidth(0)
, mHeight(0)
{
	// Empty
}

X264Base::~X264Base()
{
	// Empty
}
//END

// ----------------------------------------------------------------------------
// ------------------------- Public -------------------------------------------
// ----------------------------------------------------------------------------

void X264Base::setProfile(const std::string &profile, double fps){
	mProfile   = profile;
	mFps       = fps;
}

std::vector<char> *X264Base::streamingHeader(int width, int height){
	mWidth       = width;
	mHeight      = height;
	mEncoder     = createEncoder(width, height);
	return mAvcC.get();
}

void X264Base::createTransition(
std::queue<boost::shared_ptr<Image> > mSequenceImages, double fps){
	if(fps == 0){
		errorSig(GreenflowErrors::eDurationError);
		return;
	}
	//std::cerr << "X264Base::createTransition Images in list: " 
	//<< mSequenceImages.size() << std::endl;
	
	mClusterReady = false;
	delete(mCluster);
	mCluster = new RawCluster();
	boost::shared_ptr<Image> image = mSequenceImages.front();
	unsigned int secondsToShow     = image->secondsToShow() / 1000;
	unsigned int sequences         = mSequenceImages.size();
	double imageDisplayTime        = fps / sequences *
	                                 secondsToShow;
	//std::cerr << secondsToShow << "d " << frameDuration << " " << imageDisplayTime << std::endl;
	for(unsigned int i = 0; i < sequences; ++i){
		boost::shared_ptr<Image> image = mSequenceImages.front();
		mSequenceImages.pop();

		for(unsigned int j = 0; j < imageDisplayTime; ++j){
			transitionCluster(image);
		}
	}
	mClusterReady = true;
	doneSig();
	std::ofstream outputFile;
	outputFile.open("/tmp/GreenflowX264Log.log", 
		std::ofstream::out | std::ofstream::app);
	outputFile << "CREATE TRANSTION" << std::endl;
	outputFile.close();
	//std::cerr << "Close Encoder" << std::endl;
}

void X264Base::cluster(boost::shared_ptr<Image> image, double fps){
	//std::cerr << "CLUSTER" << std::endl;
	if(fps == 0){
		errorSig(GreenflowErrors::eDurationError);
		return;
	}
	mClusterReady = false;
	unsigned int secondsToShow = image->secondsToShow() / 1000;
	PicIn picIn(image);
	delete(mCluster);
	mCluster = new RawCluster();
	double displayTime = fps * secondsToShow;
	//std::cerr << "X264Base::cluster " <<  secondsToShow << " " << frameDuration << " " << displayTime << " " << std::endl;
	for(unsigned int i = 0; i < displayTime; ++i){
		int frame_size =
			x264_encoder_encode(mEncoder, &mNals, &mINals, picIn.picIn(), &mPicOut);
		if(frame_size >= 0){
			mCluster->addData((char *)mNals[0].p_payload,frame_size, mPicOut.i_type);
		}else{
			delete(mCluster);
			mCluster = 0;
			//std::cerr << "EMPTY FRAME" << std::endl;
			errorSig(GreenflowErrors::eEmptyFrameError);
		}
	}
	mClusterReady = true;
	//std::cerr << "DONE SIG" << std::endl;
	doneSig();
}

void X264Base::transitionCluster(boost::shared_ptr<Image> image){
	PicIn picIn(image);
	int frame_size =
		x264_encoder_encode(mEncoder, &mNals, &mINals, picIn.picIn(), &mPicOut);
	if(frame_size >= 0){
		mCluster->addData((char *)mNals[0].p_payload,frame_size, mPicOut.i_type);
	}else{
		//std::cerr << "X264Base::cluster Booooo" << std::endl;
		delete(mCluster);
		mCluster = 0;
		errorSig(GreenflowErrors::eEmptyFrameError);
	}
}


std::vector<std::vector<char> > *X264Base::getCluster(void) const{
	if(mCluster && mClusterReady){
		return mCluster->getCluster();
	}
	X264Exception error;
	error.setError(GreenflowErrors::eClusterNotReady);
	throw(error);
}

void X264Base::openEncoder(void){
	mEncoder = createEncoder(mWidth,mHeight);
	std::ofstream outputFile;
	outputFile.open("/tmp/GreenflowX264Log.log", 
		std::ofstream::out | std::ofstream::app);
	outputFile << "OPEN_ENCODER" << std::endl;
	outputFile.close();
	//std::cerr << "Close Encoder" << std::endl;
}

// ----------------------------------------------------------------------------
// ------------------------- Private ------------------------------------------
// ----------------------------------------------------------------------------

x264_t *X264Base::createEncoder(int width, int height){
	X264Params *param = new X264Params(mFps);
	param->size(width,height);
	
	x264_param_apply_profile(param->param(), mProfile.c_str());
	x264_t* encoder = x264_encoder_open(param->param());
	x264_encoder_parameters(encoder, param->param());
	
	mAvcC = x264Headers(param->param(),encoder);
	delete(param);
	param = 0;
	
	std::ofstream outputFile;
	outputFile.open("/tmp/GreenflowX264Log.log", 
		std::ofstream::out | std::ofstream::app);
	outputFile << "CREATE_ENCODER" << std::endl;
	outputFile.close();
	//std::cerr << "Close Encoder" << std::endl;
	
	return encoder;
}


void X264Base::closeEncoder(void){
	x264_encoder_close(mEncoder);
	delete(mCluster);
	mCluster = 0;
	mClusterReady = false;
	std::ofstream outputFile;
	outputFile.open("/tmp/GreenflowX264Log.log", 
		std::ofstream::out | std::ofstream::app);
	outputFile << "OPEN_ENCODER2" << std::endl;
	outputFile.close();
	//std::cerr << "Close Encoder" << std::endl;
}

X264Base::charVecPtr X264Base::x264Headers(x264_param_t *param,
x264_t *encoder){
	X264Base::charVecPtr avcC(new std::vector<char>);
	if(!param->b_repeat_headers){
		// Write SPS/PPS/SEI
		x264_nal_t *headers;
		int i_nal;
		x264_encoder_headers(encoder, &headers, &i_nal);
		int sps_size = headers[0].i_payload - 4;
		int pps_size = headers[1].i_payload - 4;
		// int sei_size = headers[2].i_payload;
		uint8_t *sps = headers[0].p_payload + 4;
		uint8_t *pps = headers[1].p_payload + 4;
		//uint8_t *sei = headers[2].p_payload;
		// if( !p_mkv->width || !p_mkv->height ||
		//  !p_mkv->d_width || !p_mkv->d_height )
		//  return -1;
		int avcC_len = 5 + 1 + 2 + sps_size + 1 + 2 + pps_size;
		
		avcC->resize(avcC_len);
		(*avcC)[0] = 1;
		(*avcC)[1] = sps[1];
		(*avcC)[2] = sps[2];
		(*avcC)[3] = sps[3];
		(*avcC)[4] = 0xff; // nalu size length is four bytes
		(*avcC)[5] = 0xe1; // one sps

		(*avcC)[6] = sps_size >> 8;
		(*avcC)[7] = sps_size;

		memcpy( avcC->data()+8, sps, sps_size );

		(*avcC)[8+sps_size] = 1; // one pps
		(*avcC)[9+sps_size] = pps_size >> 8;
		(*avcC)[10+sps_size] = pps_size;

		memcpy( avcC->data()+11+sps_size, pps, pps_size );
	}
	return avcC;
}










