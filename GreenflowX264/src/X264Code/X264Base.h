/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef _X264BASE_H_
#define _X264BASE_H_

class PicIn;

#include "../Image.h"
#include "../RawCluster.h"
#include "../../../CommonCode/Commands.h"

#include <memory>
#include <string>
#include <queue>
#include <boost/signals2.hpp>

	namespace s = boost::signals2;

#include <stdint.h>
extern "C"{
	#include "x264.h"
}

/**
	@class X264Base
	@brief Contains the interface to the x264 lib.
	This is the interface to the x264 lib. Most stuff in here is bound to
	change as soon as the x264 parameters an the encoder are better understood.
*/
class X264Base
{

	typedef boost::shared_ptr<std::vector<char> > charVecPtr;

public:

	X264Base();

 ~X264Base();
 	
 	/**
		Creates raw mkv header for a movie with width/height. Must be executed
		before an image can be added to cluster.
 	*/
	std::vector<char> *streamingHeader(int width, int height);
	
	/**
		Takes am image in Image format and the frameDuration and feeds the
		x264 lib with it. As soon as the frame is encoded the doneSig is emitted.
		Before usage create encode with 'streamingHeader'.
	*/
	void cluster(boost::shared_ptr<Image> image, double fps);
	
	void transitionCluster(boost::shared_ptr<Image> image);

	/**
		Parameters for x264 initialisation
	*/
	void setProfile(const std::string &profile, double fps);

	/**
		Allows to access the encoded cluster as soon es it is created.
		In case of an error, a instance of X264Exception is thrown.
	*/
	std::vector<std::vector<char> > *getCluster(void) const;

	void closeEncoder(void);

	void openEncoder(void);

	void createTransition(std::queue<boost::shared_ptr<Image> > mSequenceImages, double fps);

private:

	// --- Initialised in Constructor initialisation list -----------------------
	std::string             mProfile;
	x264_t          *       mEncoder;
	double                  mFps;
	RawCluster      *       mCluster;
	bool                    mClusterReady;
	x264_nal_t      *       mNals;
	int                     mINals;
	int                     mWidth;
	int                     mHeight;
	// --------------------------------------------------------------------------

	x264_picture_t          mPicOut;
	charVecPtr              mAvcC;

	/**
		Initialises several x264_param_t parameters. Most parameters are fixed,
		which probable will have to change. Only image dependant params are
		width and height.
	*/
	x264_t *createEncoder(int width, int height);

	/**
		Creates x264Headers using the parameter set created by 'createEncoder'.
	*/
	charVecPtr x264Headers(x264_param_t *param, x264_t *encoder);
	
//signals:

public:

	s::signal<void (GreenflowErrors::Error)> errorSig;

	s::signal<void (void)> doneSig;

};

#endif 
