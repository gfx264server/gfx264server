/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "./UdpServer.h"
#include "./ResponseBuffer.h"
#include "./PackageHandler.h"
#include "./StringFunctions.h"
#include "./TrafficTimerSingleton.h"
#include "./Logger.h"
#include "../../CommonCode/Commands.h"

#include <boost/bind.hpp>
#include <boost/asio/ip/address_v4.hpp>
#include <vector>

using boost::asio::ip::address_v4;
using boost::asio::buffer;
typedef boost::asio::ip::udp::endpoint endpoint;

//BEGIN Constructor/Destructor
UdpServer::UdpServer(int port, const std::string &addr, io_service &io_service)
: mStrand(io_service)
, mSocket(io_service, udp::endpoint(address_v4().from_string(addr), port))
, mTimer(io_service)
, mResponseBuffer(new ResponseBuffer(mSocket, mRemoteEndpoint))
, mPackageHandler(new PackageHandler(mResponseBuffer))
, mTrafficTimer(TrafficTimerSingleton::instance())
{
	//Logger::log(__PRETTY_FUNCTION__,"START");
	mTrafficTimer->setTimer(&mTimer);
	mTrafficTimer->expiresFromNow(180);
	startReceive();
}
	
UdpServer::~UdpServer()
{
	//Empty
}
//END

// ----------------------------------------------------------------------------
// ------------------------- Private ------------------------------------------
// ----------------------------------------------------------------------------

void UdpServer::startReceive(void){
	mRecvBuffer.resize(GreenflowX264Commands::sendBufferSize());
	mSocket.async_receive_from(
		buffer(mRecvBuffer, mRecvBuffer.size()),
		mRemoteEndpoint,
		boost::bind(&UdpServer::handleReceive, this,
		boost::asio::placeholders::error,
		boost::asio::placeholders::bytes_transferred)
	);
}

void UdpServer::handleReceive(const boost::system::error_code &error,
std::size_t bytes_transferred)
{
	if(!packageOk(bytes_transferred)){
		return;
	}

	int             command = mRecvBuffer[GreenflowX264Commands::COMMAND];
	boost::uint16_t   seqNo = sequenceNo();
	removeHeader(bytes_transferred);

	if (!error || error == boost::asio::error::message_size){
		mPackageHandler->process(command, seqNo, mRecvBuffer);
		startReceive();
	}else{
		//std::cerr << "ERROR: " << error << std::endl;
		mResponseBuffer->sendErrorMessage(GreenflowErrors::boostAsioError,
			StringFunctions::toHexString(error.value()));
	}
}

bool UdpServer::packageOk(std::size_t bytes_transferred) const{
	if(bytes_transferred < mResponseBuffer->headerSize()){
		mResponseBuffer->sendMessage(GreenflowX264Commands::corruptPackage);
		//std::cerr << "Incoming package is corrupt." << std::endl;
		return false;
	}
	if(!mResponseBuffer->magicOk(mRecvBuffer)){
		mResponseBuffer->sendMessage(GreenflowX264Commands::wrongHash);
		//std::cerr << "Incoming package has wrong hash" << std::endl;
		return false;
	}
	return true;
}

boost::uint16_t UdpServer::sequenceNo(void) const{
	boost::uint16_t seqNo =
		(mRecvBuffer[GreenflowX264Commands::SEQUENCE]  <<    8) | 
		(mRecvBuffer[GreenflowX264Commands::SEQUENCE+1] & 0xFF);
	seqNo = ntohs(seqNo);
	return seqNo;
}

void UdpServer::removeHeader(std::size_t bytes_transferred){
	mRecvBuffer.erase(mRecvBuffer.begin(),mRecvBuffer.begin()+
		mResponseBuffer->headerSize());	
	mRecvBuffer.resize(bytes_transferred-mResponseBuffer->headerSize());
}

