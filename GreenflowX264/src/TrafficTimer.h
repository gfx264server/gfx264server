/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _TRAFFICTIMER_H_
#define _TRAFFICTIMER_H_

#include <boost/asio.hpp>
#include <boost/signals2.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

	namespace s = boost::signals2;

/**
	@class TrafficTimer
	@brief Global timer used to detect 'lost' connections.
	Can be used to repeat a command after a certain time of none response.
	Necessary since UPD packages can get lost.
*/
class TrafficTimer
{
public:

	TrafficTimer();

	~TrafficTimer();

	void setTimer(boost::asio::deadline_timer *timer);

	void expiresFromNow(int seconds);

	void cancel(void);

private:

	// --- Initialised in Constructor initialisation list -----------------------
	boost::asio::deadline_timer        *    mTimer;
	// --------------------------------------------------------------------------

	void onTimeout(const boost::system::error_code& e);

public:

// signals

	s::signal<void (void)> timeoutSig;

};

#endif
