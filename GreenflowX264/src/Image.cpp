/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "./Image.h"

Image::Image(imagePtr image, int width, int height)
: mImage(image)
, mWidth(width)
, mHeight(height)
, mSecondsToShow(2)
{
	//Empty
}

Image::~Image()
{
	//Empty
}

// ----------------------------------------------------------------------------
// ------------------------- Public ------------------------------------------- 
// ----------------------------------------------------------------------------

uint8_t *Image::data(void){
	return reinterpret_cast<uint8_t *>(mImage->data());
}

int Image::width(void) const{
	return mWidth;
};

int Image::height(void) const{
	return mHeight;
}

void Image::setSecondsToShow(unsigned int seconds){
	mSecondsToShow = seconds;
}

unsigned int Image::secondsToShow(void) const{
	return mSecondsToShow;
}

void Image::setSequence(bool sequence){
	mIsSequence = sequence;
}

bool Image::isSequence(void) const{
	return mIsSequence;
}

void Image::setLastFrame(bool lastFrame){
	mLastFrame = lastFrame;
}

bool Image::isLastFrame(void) const{
	return mLastFrame;
}
	
int Image::size(){
	return mImage->size();
}

