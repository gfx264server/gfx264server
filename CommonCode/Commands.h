/*
  This file is part of GreenflowX264Server
  copyright   : (C) 2011 Greenflow AS
  email       : info@greenflow-solutions.com

  GreenflowX264Server is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  GreenflowX264Server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef _COMMANDS_H_
#define _COMMANDS_H_

namespace GreenflowX264Commands{
	
	enum Commands{
		  undefinedCommand
		, stopSending
		, startSending
		, nextSending
		, doneSending
		, requestMissing
		, initConnection
		, createEncoder
		, sendFrame
		, nextPackage
		, startTransmission
		, stopTransmission
		, requestPackage
		, replyInit
		, encoderReply
		, startEncodedFrame
		, stopTimer
		, sendEncodedBlock
		, nextEncodedBlock
		, sendLastEncodedBlock
		, wrongHash
		, doneReceive
		, corruptPackage
		, unknownCommand
		, lostPackage
		, killServer
		, closeEncoder
		, sendOpen
		, sendNextImage
		, errorMessage
	};
	
	enum Status{
			undefinedStatus
		, receiving
		, sending
	};

	inline int sendBufferSize(void) { return 512; };
	static const int COMMAND  = 20;
	static const int SEQUENCE = 21;
};

namespace GreenflowErrors{

	enum Error{
		  eUndefinedError
		, eDurationError
		, eEmptyFrameError
		, eClusterNotReady
		, boostAsioError
		, corruptPackageError
	};
}

#endif
